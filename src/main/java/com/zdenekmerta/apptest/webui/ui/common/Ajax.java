package com.zdenekmerta.apptest.webui.ui.common;

import org.springframework.web.context.request.WebRequest;

/**
 * Funkce pro praci s Ajaxem.
 * 
 * @author Zdenek Merta
 */
public final class Ajax {
	public static boolean isAjaxRequest(String requestedWith) {
		return requestedWith != null ? "XMLHttpRequest".equals(requestedWith) : false;
	}
	
	public static boolean isAjaxUploadRequest(WebRequest webRequest) {
		return webRequest.getParameter("ajaxUpload") != null;
	}
}