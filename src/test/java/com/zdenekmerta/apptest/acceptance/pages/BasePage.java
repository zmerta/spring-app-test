package com.zdenekmerta.apptest.acceptance.pages;

import org.jbehave.web.selenium.WebDriverProvider;
import org.openqa.selenium.WebDriver;

/**
 * Predek vsech aplikacnich stranek.
 * 
 * @author Zdenek Merta
 */
public abstract class BasePage {
	private final WebDriverProvider webDriverProvider;
	private final String applicationUrl;

	public BasePage(WebDriverProvider webDriverProvider, String applicationUrl) {
		this.webDriverProvider = webDriverProvider;
		this.applicationUrl = applicationUrl;
	}
	
	/**
	 * Otevre aplikacni stranku.
	 */
	public void open() {
		getWebDriver().get(getUrl());
	}
	
	/**
	 * Overi, jestli je aplikacni stranka aktivni.
	 */
	public boolean isActive() {
		return getWebDriver().getTitle().startsWith(getTitlePrefix());
	}
	
	/**
	 * Vraci URL aplikacni stranky.
	 * 
	 * @return URL aplikacni stranky
	 */
	protected String getUrl() {
		return getApplicationUrl() + getRelativeUrl();
	}
	
	/**
	 * Vraci relativni URL aplikacni stranky.
	 * 
	 * @return relativni URL aplikacni stranky
	 */
	protected abstract String getRelativeUrl();
	
	/**
	 * Vraci prefix titulku stranky.
	 * 
	 * @return prefix titulku aplikacni stranky.
	 */
	protected abstract String getTitlePrefix();

	/**
	 * Vraci URL aplikace.
	 * 
	 * @return URL aplikace
	 */
	protected String getApplicationUrl() {
		return applicationUrl;
	}
	
	protected WebDriver getWebDriver() {
		return webDriverProvider.get();
	}
}