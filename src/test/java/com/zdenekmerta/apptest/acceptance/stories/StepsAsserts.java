package com.zdenekmerta.apptest.acceptance.stories;

import static org.junit.Assert.fail;

import com.zdenekmerta.apptest.acceptance.pages.BasePage;

/**
 * Funkce pro praci s kroky.
 * 
 * @author Zdenek Merta
 */
public final class StepsAsserts {
	private StepsAsserts() {
	}
	
	public static <T extends BasePage> void assertActive(T page) {
		if (!page.isActive()) {
			fail(String.format("Page '%1s' is not active", page.getClass().getSimpleName()));
		}
	}
}