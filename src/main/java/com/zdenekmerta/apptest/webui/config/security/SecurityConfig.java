package com.zdenekmerta.apptest.webui.config.security;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;

@Configuration
@ImportResource("classpath:com/zdenekmerta/apptest/webui/config/security/security.xml")
public class SecurityConfig {
}