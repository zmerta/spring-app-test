package com.zdenekmerta.apptest.webui.ui.signin;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.zdenekmerta.apptest.webui.ui.common.BaseController;

/**
 * Prihlasovani do aplikace.
 * 
 * @author Zdenek Merta
 */
@Controller
public final class SigninController extends BaseController {
	private static final String TEMPLATE_ROOT_PATH = "signin";
	
	@RequestMapping(value = "/signin", method = RequestMethod.GET)
	public String signin() {
		return getTemplatePath("signin");
	}

	@Override
	protected String getTemplateRootPath() {
		return TEMPLATE_ROOT_PATH;
	}
}