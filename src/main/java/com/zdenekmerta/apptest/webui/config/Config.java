package com.zdenekmerta.apptest.webui.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.PropertySource;

/**
 * Konfigurace aplikace.
 * 
 * @author Zdenek Merta
 */
@Configuration
@ComponentScan(basePackages = "com.zdenekmerta.apptest")
public class Config {
	@Configuration
	@Profile(Production.NAME)
	@PropertySource("classpath:production.properties")
	public static class Production {
		public static final String NAME = "production";
	}
	
	@Configuration
	@Profile(Development.NAME)
	@PropertySource("classpath:development.properties")
	public static class Development {
		public static final String NAME = "development";
	}
}