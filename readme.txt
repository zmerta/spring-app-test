KONFIGURACE:
- promenne prostredi pro nastaveni WebDriveru:
	browser=chrome
	webdriver.chrome.driver="c:/Program Files/chromedriver/chromedriver.exe"
	application.url=http://localhost:8080/spring-app-test/

TODO:

JBehave:
- promyslet pouziti jako v http://ivanz.com/2011/05/25/java-bdd-with-jbehave-and-watij-in-eclipse-with-junit/

Validace:
- konfigurace MessageSorce
- chybove hlasky dat do springovych properties 

Logovani:
- logovani pres BeanPostProcessor http://crazygui.wordpress.com/2011/12/12/configure-logback-logging-with-spring/

Databaze
- pridat praci s databazi
- pridat migrace pomoci http://code.google.com/p/flyway/