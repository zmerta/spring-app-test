package com.zdenekmerta.apptest.webui.security;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class SimpleAuthenticationProvider implements AuthenticationProvider {
	private final static Set<String> validUsernames = new HashSet<String>(Arrays.asList("admin", "guest"));
	
	@Override
	public Authentication authenticate(Authentication authentication) throws AuthenticationException {
		UsernamePasswordAuthenticationToken authenticationToken = (UsernamePasswordAuthenticationToken) authentication;
		
		if (!isValidUsername(authenticationToken.getName())) {
			throw new UsernameNotFoundException(authenticationToken.getName());
		}
		
		return createAuthentication(authenticationToken);
	}

	@Override
	public boolean supports(Class<?> authentication) {
		return UsernamePasswordAuthenticationToken.class.equals(authentication);
	}
	
	private boolean isValidUsername(String username) {
		return validUsernames.contains(username);
	}
	
	private Authentication createAuthentication(UsernamePasswordAuthenticationToken original) {
		List<GrantedAuthority> authorities = null;
		UsernamePasswordAuthenticationToken authenticated = new UsernamePasswordAuthenticationToken(
				original.getName(), null, authorities);
		authenticated.setDetails(original.getDetails());
		return authenticated;		
	}
}