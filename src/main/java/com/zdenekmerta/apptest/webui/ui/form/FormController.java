package com.zdenekmerta.apptest.webui.ui.form;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.zdenekmerta.apptest.webui.ui.common.BaseController;

/**
 * Demonstracni formular
 * 
 * @author Zdenek Merta
 */
@Controller
public final class FormController extends BaseController {
	private static final String TEMPLATE_ROOT_PATH = "form";
	
	private static final Logger log = LoggerFactory.getLogger(FormController.class);
	
	@RequestMapping(value = "/form", method = RequestMethod.GET)
	public String showForm(@ModelAttribute("form") Form form) {
		return getTemplatePath("form");
	}
	
	@RequestMapping(value = "/form", method = RequestMethod.POST)
	public String submitForm(
			@ModelAttribute("form") @Validated Form form, BindingResult formBinding,
			@RequestHeader(value = "X-Requested-With", required = false) String requestedWith,
			RedirectAttributes redirectAttributes) {
		if (!formBinding.hasErrors()) {
			log.info("Form sent");
			
			redirectAttributes.addFlashAttribute("formResult", "OK");
			return redirectTo("success");
		}
		return showForm(form);
	}
	
	@RequestMapping(value = "/success")
	public String success() {
		return getTemplatePath("success");
	}
	
	@Override
	protected String getTemplateRootPath() {
		return TEMPLATE_ROOT_PATH;
	}
}