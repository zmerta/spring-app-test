package com.zdenekmerta.apptest.webui.config.web;

import java.util.Collections;

import javax.inject.Inject;

import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.core.env.Environment;
import org.springframework.validation.Validator;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.thymeleaf.spring3.SpringTemplateEngine;
import org.thymeleaf.spring3.messageresolver.SpringMessageResolver;
import org.thymeleaf.spring3.view.ThymeleafViewResolver;
import org.thymeleaf.templateresolver.ServletContextTemplateResolver;

import com.zdenekmerta.apptest.webui.config.Config;

@Configuration
@EnableWebMvc
@ComponentScan(basePackages = "com.zdenekmerta.apptest.webui")
public class WebConfig extends WebMvcConfigurerAdapter {
	private static final String ENCODING = "UTF-8";
	
	@Inject
	private Environment environment;
	
	@Override
	public Validator getValidator() {
		LocalValidatorFactoryBean validator = new LocalValidatorFactoryBean();
		//validator.setValidationMessageSource(messageSource()); // TODO prekladove mapy do springovych properties
		return validator;
	}
	
	@Bean
	public ViewResolver viewResolver() {
		ServletContextTemplateResolver templateResolver = new ServletContextTemplateResolver();
		templateResolver.setCacheable(true);
		templateResolver.setPrefix("/WEB-INF/templates/");
		templateResolver.setSuffix(".html");
		templateResolver.setTemplateMode("HTML5");
		templateResolver.setCharacterEncoding(ENCODING);
		if (environment.acceptsProfiles(Config.Development.NAME)) {
			templateResolver.setCacheTTLMs(0L);
		}
		
		SpringTemplateEngine templateEngine = new SpringTemplateEngine();
		templateEngine.setTemplateResolver(templateResolver);
		
		SpringMessageResolver springMessageResolver = new SpringMessageResolver();
		springMessageResolver.setMessageSource(messageSource());
		templateEngine.setDefaultMessageResolvers(Collections.singleton(springMessageResolver));
		
		ThymeleafViewResolver viewResolver = new ThymeleafViewResolver();
		viewResolver.setTemplateEngine(templateEngine);
		viewResolver.setCharacterEncoding("UTF-8");
		return viewResolver;
	}
	
	@Bean
	public MessageSource messageSource() {
		ReloadableResourceBundleMessageSource messageSource = new ReloadableResourceBundleMessageSource();
		messageSource.setDefaultEncoding(ENCODING);
		messageSource.setBasenames("classpath:messages");
		if (environment.acceptsProfiles(Config.Development.NAME)) {
			messageSource.setCacheSeconds(0);
			messageSource.setCacheSeconds(0);
		}
		
		return messageSource;
	}
}