package com.zdenekmerta.apptest.acceptance.pages;

import java.lang.reflect.Constructor;
import java.util.HashMap;
import java.util.Map;

import org.jbehave.web.selenium.WebDriverProvider;

import com.zdenekmerta.apptest.acceptance.pages.home.Home;
import com.zdenekmerta.apptest.acceptance.pages.signin.Signin;

/**
 * Tovarna aplikacnich stranek.
 * 
 * @author Zdenek Merta
 */
public final class Pages {
	private final WebDriverProvider webDriverProvider;
	private final String applicationUrl;
	
	/**
	 * Cache aplikacnich stranek.
	 */
	private final Cache cache = new Cache();
	
	public Pages(WebDriverProvider webDriverProvider) {
		this(webDriverProvider, System.getProperty("application.url", "http://localhost:8080"));
	}
	
	public Pages(WebDriverProvider webDriverProvider, String applicationUrl) {
		this.webDriverProvider = webDriverProvider;
		this.applicationUrl = applicationUrl;
	}
	
	/**
	 * Vytvori prihlasovaci stranku.
	 * 
	 * @return prihlasovaci stranku
	 */
	public Signin signin() {
		return createPage(Signin.class);
	}
	
	/**
	 * Vytvori domovskou stranku.
	 * 
	 * @return domovskou stranku
	 */
	public Home home() {
		return createPage(Home.class);
	}
	
	/**
	 * Vytvori aplikacni stranku.
	 * 
	 * @param type typ aplikacni stranky
	 * @return aplikacni stranku
	 */
	private <T extends BasePage> T createPage(Class<T> type) {
		T page = cache.get(type);
		if (page == null) {
			try {
				Constructor<T> constructor = type.getConstructor(WebDriverProvider.class, String.class);
				page = constructor.newInstance(webDriverProvider, applicationUrl);
				cache.put(page);
			} catch (Exception e) {
				throw new RuntimeException(String.format("Error creating page '%1s'", type.getSimpleName()), e);
			}
		}
		return page;
	}
	
	/**
	 * Cache aplikacnich stranek.
	 * 
	 * @author Zdenek Merta
	 */
	static class Cache {
		private final Map<Class<?>, BasePage> cache = new HashMap<Class<?>, BasePage>();
		
		/**
		 * Prida aplikacni stranku do cache.
		 * 
		 * @param page aplikacni stranka
		 */
		public <T extends BasePage> void put(T page) {
			cache.put(page.getClass(), page);
		}
		
		/**
		 * Vraci aplikacni stranku z cache.
		 * 
		 * @param type typ aplikacni stranky
		 * @return vraci aplikacni stranku z cache nebo <code>null</code> pokud neni v cache
		 */
		public <T extends BasePage> T get(Class<T> type) {
			BasePage page = cache.get(type);
			return page != null ? type.cast(page) : null;
		}
	}
}