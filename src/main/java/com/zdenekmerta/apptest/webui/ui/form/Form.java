package com.zdenekmerta.apptest.webui.ui.form;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;

/**
 * Demonstracni formular.
 * 
 * @author Zdenek Merta
 */
public class Form {
	@Length(min = 1, max = 64)
	private String firstname;
	
	@Length(min = 1, max = 64)
	private String lastname;
	
	@NotEmpty @Email
	private String email;
	
	public String getFirstname() {
		return firstname;
	}
	
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}
	
	public String getLastname() {
		return lastname;
	}
	
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	
	public String getEmail() {
		return email;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}
}