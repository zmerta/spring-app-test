package com.zdenekmerta.apptest.acceptance.pages.signin;

import org.jbehave.web.selenium.WebDriverProvider;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.zdenekmerta.apptest.acceptance.pages.BasePage;

/**
 * Prihlasovaci stranka aplikace.
 * 
 * @author Zdenek Merta
 */
public class Signin extends BasePage {
	public Signin(WebDriverProvider webDriverProvider, String applicationUrl) {
		super(webDriverProvider, applicationUrl);
	}

	public void signin(String username, String password) {
		WebDriver driver = getWebDriver();
		
		driver.findElement(By.id("username")).sendKeys(username);
		driver.findElement(By.id("password")).sendKeys(password);
		driver.findElement(By.id("signin")).click();
	}

	@Override
	protected String getRelativeUrl() {
		return "/signin";
	}
	
	@Override
	protected String getTitlePrefix() {
		return "Spring Application Test: signin";
	}
}