package com.zdenekmerta.apptest.webui.config;

import java.util.EnumSet;

import javax.servlet.DispatcherType;
import javax.servlet.FilterRegistration.Dynamic;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration;

import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.filter.CharacterEncodingFilter;
import org.springframework.web.filter.DelegatingFilterProxy;
import org.springframework.web.servlet.DispatcherServlet;

import com.zdenekmerta.apptest.webui.config.web.WebConfig;

/**
 * Inicializace aplikace.
 * 
 * Pozor v Tomcatu verze <= 7.0.14 je <a href="https://issues.apache.org/bugzilla/show_bug.cgi?id=51278">chyba</a> pri mapovani
 * servletu na <code>/</code>.
 * 
 * @author Zdenek Merta
 */
public class Initializer implements WebApplicationInitializer {
	private static final String SECURITY_FILTER_MAPPING = "/*";
	
	private static final String DISPATCHER_SERVLET_NAME = "dispatcher";
    private static final String DISPATCHER_SERVLET_MAPPING = "/";

	@Override
	public void onStartup(ServletContext servletContext) throws ServletException {
		// Spring Security filtr - konfiguruje se pomoci xml, Java konfigurace je celkem slozita 
		Dynamic securityFilter = servletContext.addFilter("springSecurityFilterChain", DelegatingFilterProxy.class);
		securityFilter.addMappingForUrlPatterns(EnumSet.allOf(DispatcherType.class), false, SECURITY_FILTER_MAPPING);
		
		CharacterEncodingFilter characterEncodingFilter = new CharacterEncodingFilter();
		characterEncodingFilter.setEncoding("UTF-8");
		
		Dynamic dynamicCharacterEncodingFilter = servletContext.addFilter("characterEncodingFilter", characterEncodingFilter);
		dynamicCharacterEncodingFilter.addMappingForUrlPatterns(EnumSet.allOf(DispatcherType.class), false, "/*");
		
		// Root context
		AnnotationConfigWebApplicationContext rootContext = new AnnotationConfigWebApplicationContext();
		rootContext.register(WebConfig.class);
		
		// Dispatcher servlet
		ServletRegistration.Dynamic dispatcher = servletContext.addServlet(DISPATCHER_SERVLET_NAME,
				new DispatcherServlet(rootContext));
        dispatcher.setLoadOnStartup(1);
        dispatcher.addMapping(DISPATCHER_SERVLET_MAPPING);
        
        servletContext.addListener(new ContextLoaderListener(rootContext));
	}	
}