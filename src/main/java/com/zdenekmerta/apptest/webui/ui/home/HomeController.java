package com.zdenekmerta.apptest.webui.ui.home;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.zdenekmerta.apptest.webui.ui.common.BaseController;

/**
 * Domovska stranka aplikace.
 * 
 * @author Zdenek Merta
 */
@Controller
public class HomeController extends BaseController {
	private static final String TEMPLATE_ROOT_PATH = "home";
	
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String showHome() {
		return getTemplatePath("home");
	}

	@Override
	protected String getTemplateRootPath() {
		return TEMPLATE_ROOT_PATH;
	}
}