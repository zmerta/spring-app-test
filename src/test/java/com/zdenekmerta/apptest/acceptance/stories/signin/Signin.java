package com.zdenekmerta.apptest.acceptance.stories.signin;

import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.junit.Assert;

import com.zdenekmerta.apptest.acceptance.pages.Pages;
import com.zdenekmerta.apptest.acceptance.pages.home.Home;
import com.zdenekmerta.apptest.acceptance.stories.StepsAsserts;
import com.zdenekmerta.apptest.acceptance.stories.StoryBase;

public class Signin extends StoryBase {
	@Given("uživatel je na přihlašovací stránce")
	public void userIsOnSigninPage() {
		getPages().signin().open();
	}
	
	@When("vyplní přihlašovací jméno $username a heslo $password a stiskne tlačítko přihlásit")
	public void userSignsIn(String username, String password) {
		getPages().signin().signin(username, password);
	}
	
	@Then("je přihlášen jako uživatel $username a je přesměrován na domovskou stránku")
	public void userIsSignedInAndIsOnHomePage(String username) {
		Home home = getPages().home();
		
		Assert.assertTrue(home.isUserAuthorized(username));
		StepsAsserts.assertActive(home);
	}
	
	@Then("je přesměrován na přihlašovací stránku")
	public void userIsRedirectedToSigninPage() {
		StepsAsserts.assertActive(getPages().signin());
	}
}