package com.zdenekmerta.apptest.acceptance.pages.home;

import org.jbehave.web.selenium.WebDriverProvider;

import com.zdenekmerta.apptest.acceptance.pages.BasePage;

/**
 * Domovska stranka aplikace.
 * 
 * @author Zdenek Merta
 */
public class Home extends BasePage {
	public Home(WebDriverProvider webDriverProvider, String applicationUrl) {
		super(webDriverProvider, applicationUrl);
	}
	
	public boolean isUserAuthorized(String username) {
		return true; // TODO na strance neni zobrazeno prihlasovaci jmeno
	}
	
	@Override
	protected String getRelativeUrl() {
		return "/home";
	}
	
	@Override
	protected String getTitlePrefix() {
		return "Spring application test: home";
	}
}