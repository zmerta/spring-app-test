package com.zdenekmerta.apptest.webui.ui.common;

/**
 * Abstraktni predek controlleru.
 * 
 * @author Zdenek Merta
 */
public abstract class BaseController {
	/**
	 * Vraci cestu ke korenove slozce sablon controlleru.
	 * 
	 * @return cestu ke korenove slozce sablon controlleru
	 */
	protected abstract String getTemplateRootPath();
	
	/**
	 * Vraci cestu k sablone.
	 * <p>
	 * Cesta je relativni ke korenove slozce sablon controlleru.
	 * 
	 * @param template cesta k sablone
	 * @return cestu k sablone
	 */
	protected String getTemplatePath(String template) {
		return new StringBuilder(getTemplateRootPath()).append('/').append(template).toString();
	}
	
	/**
	 * Vraci cestu k presmerovani.
	 * 
	 * @param path cesta
	 * @return cestu k presmerovani
	 */
	protected String redirectTo(String path) {
		return "redirect:/" + path;
	}
}