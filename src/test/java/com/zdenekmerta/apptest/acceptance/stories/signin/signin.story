Scenario: Přihlášení uživatele s platným přihlašovacím jménem a heslem

Given uživatel je na přihlašovací stránce
When vyplní přihlašovací jméno admin a heslo test a stiskne tlačítko přihlásit
Then je přihlášen jako uživatel admin a je přesměrován na domovskou stránku

Scenario: Přihlášení uživatele s neplatným přihlašovacím jménem nebo heslem

Given uživatel je na přihlašovací stránce
When vyplní přihlašovací jméno nobody a heslo test a stiskne tlačítko přihlásit
Then je přesměrován na přihlašovací stránku